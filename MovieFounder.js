const http = require('http');
const imdb = require('imdb-api');
const moment = require('moment');

//config values
let imdbApiKey = process.env.OMDB_API || 'c797063b';

class MovieFounder {

    constructor(program, date) {
        this.program = program;
        this.date = date;
    }

    search() {
        let formattedDate = moment(this.date).format('DD.MM.YYYY');

        let requestUrl = `http://www.tvin.si/script/program_epg?date=${formattedDate}&prog=${this.program}`;
        
        http.get(requestUrl, (res) => {
            res.on('data', (chunk) => {
                var responseJSON = JSON.parse(chunk);

                if (responseJSON.response == 'ok' && responseJSON.data.prog) {
                    let programArray = responseJSON.data.prog;

                    programArray.forEach(element => {
                        if (!movieAlreadyFound(element.title)) {
                            this.checkMovieRating(element.title);
                        }
                    });
                }
            });
        }).on('error', function (e) {
            console.log("Got error: " + e.message);
        });
    }

    checkMovieRating(title) {
        let originalTitle = title;
        title = title.replace(/[^\x00-\x7F]/g, "").toLowerCase().trim();

        let url = `http://v2.sg.media-imdb.com/suggests/${title[0]}/${encodeURIComponent(title)}.json`;

        http.get(url, (res) => {
            res.on('data', (chunk) => {
                chunk = chunk.toString();
                let firstBracket = chunk.indexOf('{');
                let lastBracket = chunk.lastIndexOf(')');
                let jsonString = chunk.substring(firstBracket, lastBracket);
                let response;
                try {
                    response = JSON.parse(jsonString);
                } catch (err) {
                    this.checkMovieRating(originalTitle);
                }

                if (response && response.d && response.d[0]) {
                    let id = response.d[0].id;
                    this.getRating(id, originalTitle);
                }
            });
        }).on('error', function (e) {
            console.log("Got error: " + e.message);
        });
    }

    getRating(movieId, movieTitle) {
        imdb.getById(movieId, { apiKey: imdbApiKey, timeout: 30000 }).then(res => {
            if (this.canPrintMovie(res) && !movieAlreadyFound(movieTitle)) {
                this.printResult(movieTitle, res);
                alreadyFoundMovies.push(movieTitle);
            }
        }).catch(err => {
            //todo 
        });
    }

    canPrintMovie(movieData) {
        return movieData.rating != 'N/A' && movieData.rating >= ratingLimit && (movieData.type == 'movie' || searchAllTypes)
    }

    printResult(title, movieData) {
        let color = 'green';

        let rating = movieData.rating;

        if (rating >= 8.5) {
            color = 'red';
        } else if (rating < 8.5 && rating > 8) {
            color = 'orange'
        }

        let releasedYear = moment(movieData.released).year();
        if (isNaN(releasedYear)) {
            releasedYear = '';
        }

        console.log(title.padEnd(35), colorfy.keyword(color)(rating), '\t', this.program.padEnd(10), moment(this.date).format('DD.MM.YYYY'), '\t', movieData.type + ', ' + releasedYear + ', genre: ' + movieData.genres.toLowerCase());
    }
}



module.exports = MovieFounder;