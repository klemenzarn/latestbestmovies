require('./bootstrap');


const MovieFounder = require('./MovieFounder');

var programs = ['HBO', 'POPTV', 'AKANAL', 'PLANETTV', 'HBO2', 'HBO3', 'CINEMAX1', 'CINEMAX2', 'POPKINO', 'TV1000', 'FOXMOVIES', 'PLANETTV'];

//todo refaktorirat zadeve, da bo obstajal razred MovieFounder pa ImdbMovieSearch, MovieFounder naj bi bil abstrakten, kjer bi ble
//implementacije TVINMovieFounder, A1MovieFounder, ki bi pa oba uporablala ImdbMovieSearch, ki bi izpisoval tako kot zdaj

let dayOffset = 7;

let dates = [];
for (let i = 0; i < dayOffset; i++) {
    let startDate = new Date();
    startDate.setDate(startDate.getDate() - i);
    dates.push(startDate);
}

console.log(`Searching movies rated ${colorfy.green(ratingLimit)} and higher...`);
console.log(colorfy.green('------------------------------------------------------------------------------------------'));    

dates.forEach(date => {
    programs.forEach(program => {
        var movieFounder = new MovieFounder(program, date);
        movieFounder.search();
    });
});

process.on('exit', () => {
    console.log(colorfy.green('------------------------------------------------------------------------------------------'));    
    console.log(`Searching movies rated ${colorfy.green(ratingLimit)} and higher finished.`);
});