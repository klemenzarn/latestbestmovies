# LatestBestMovies

Search for movies in the past week on local TV (Currently supported only Slovenian TV programs)

clone repo `git clone https://github.com/klemenzarn/LatestBestMovies`

install `npm install`

run `node index.js`

optional args:
- custom rating: `-r 6.5`
- show all content not only movies: `-a true`


example searching for movies and series by IMDB rating 8 and up:
`node index.js -r 8.0 -a true`
