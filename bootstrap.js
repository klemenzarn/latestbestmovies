require('dotenv').config();

const commandLineArgs = require('command-line-args')

var chalk = require('chalk');
global.colorfy = chalk;

global.alreadyFoundMovies = [];
global.movieAlreadyFound = function (title) {
    return global.alreadyFoundMovies.indexOf(title) > -1
}


let options = {};

try {
    const optionDefinitions = [
        { name: 'rating', alias: 'r', type: Number },
        { name: 'alltypes', alias: 'a', type: Boolean }
    ];

    options = commandLineArgs(optionDefinitions);
} catch (err) {
    console.log('error getting command line arguments');
}


let ratingLimit = options.rating || process.env.RATING || 7.5;
global.ratingLimit = ratingLimit;

let searchAllTypes = options.alltypes || process.env.SEARCH_ALL_TYPES || false
global.searchAllTypes = JSON.parse(searchAllTypes); //convert string to boolean or boolean to boolean
